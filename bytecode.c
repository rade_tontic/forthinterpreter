#include "bytecode.h"
#include "backend.h"
#include "main.h"

void halt()
{
	running = 0;
}

void add()
{
	int a = pop(&data), b = pop(&data);
	push(&returns, a + b);
}

void subtract()
{
	int a = pop(&data), b = pop(&data);
	push(&returns, a - b);
}

void multiply()
{
	int a = pop(&data), b = pop(&data);
	push(&returns, a * b);
}

void divide()
{
	int a = pop(&data), b = pop(&data);
	push(&returns, a / b);
}

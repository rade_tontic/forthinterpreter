#include "stack.h"
#include "main.h"

void stack_init(Stack *current)
{
	current->size = INITIAL_SIZE;
	current->items = (int*)malloc(sizeof(int) * current->size);
	current->tos = 0, current->nos = 1;
}
void push(Stack *current, int value) 
{
	current->items[current->tos++] = value;
	current->nos++;
}
int pop(Stack *current)
{
	current->nos--;
	return current->items[current->tos--];
}
void stack_free(Stack *current)
{
	free(current->items);
}

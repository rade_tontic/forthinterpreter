#ifndef FRONTEND_H
#define FRONTEND_H

#define LENGTH 32

typedef struct word {
	char name[LENGTH];
	int instruction;
	struct word *previous;
	void (*routine)(void);
} Word;

char *get_input(FILE *stream);
char *tokenize(char *buffer);

#endif

#ifndef MAIN_H
#define MAIN_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "backend.h"
#include "bytecode.h"
#include "stack.h"
#include "frontend.h"

extern int running;
extern Stack data, returns;

void run();
void init_stacks();
#endif

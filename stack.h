#ifndef STACK_H
#define STACK_H
#define INITIAL_SIZE 128

typedef struct stack {
	int *items, tos, nos, size;
} Stack;

void stack_init(Stack *current);
void push(Stack *current, int value);
int pop(Stack *current);
void stack_free(Stack *current);

#endif

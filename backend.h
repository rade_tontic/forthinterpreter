#ifndef BACKEND_H
#define BACKEND_H

int fetch();
void eval(int* instruction);
int* decode(int instruction);
void run();

#endif

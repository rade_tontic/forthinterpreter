#ifndef BYTECODE_H
#define BYTECODE_H

void halt();
void add();
void subtract();
void multiply();
void divide();

#endif
